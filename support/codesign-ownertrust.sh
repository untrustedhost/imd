#!/usr/bin/env bash

set -eu

# if we ever make a new codesign key, update _here_
KEY_ID=D6EEA663FD2DA21DB986FFA9ED2FDE47B76BB91A
TA_URL='https://arrjay.gitlab.io/trust-anchors/install.run'
OK_FILE='cache/codesign-ownertrust'

# install codesign-ownertrust keyring if missing
reqs=('curl' 'mktemp')

for r in ${reqs[@]} ; do
  type $r > /dev/null 2>&1 || { echo "missing $r" ; exit 1 ; }
done

# deal with gpg, preferring gpg2
gpg=
type gpg > /dev/null 2>&1 && gpg=gpg
type gpg2 > /dev/null 2>&1 && gpg=gpg2

[[ "${gpg}" ]] || { echo "I have no gpg or gpg2" ; exit 1 ; }

# find key by long id
found=0
while read -r l ; do
  IFS=: read -r -a sp <<< "$l"
  case $l in
    uid:*)
      [[ "${sp[7]}" == "${KEY_ID}" ]] && { touch "${OK_FILE}" ; exit 0 ; }
    ;;
  esac
done < <("${gpg}" --list-keys --with-colons)

# we didn't have a key, go get one
dfile=$(mktemp)
curl -L -o "${dfile}" "${TA_URL}"
chmod +x "${dfile}"
"${dfile}" codesign-ownertrust

rm "${dfile}"

touch "${OK_FILE}"
