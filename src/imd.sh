#!/usr/bin/env bash

# shellcheck source=src/conf/imd.conf
. /etc/untrustedhost/imd.conf

IMD_FILE="MDDATA.XML"
IMD_RUN="/run/imd"
IMD_MOUNT="${mount_dest}"
IMD_PATH="${IMD_PATH:-${IMD_MOUNT}/${IMD_FILE}}"
systemd_lldp="${SYSTEMD_LLDP:-true}"
systemd_usemtu="${SYSTEMD_DHCPMTU:-true}"
systemd_netifdir="${SYSTEMD_NETIFDIR:-/run/systemd/network}"
interfaces_d_dir="${INTERFACES_D:-/run/untrustedhost/interfaces.d}"
run_scripts="${RUN_SCRIPTS:-true}"

# shellcheck source=src/shelllib/imd.bash
. /usr/lib/untrustedhost/shelllib/imd.bash

export IMD_MOUNT IMD_PATH

lock_file="${IMD_RUN}/.lock"
# 0 for immidiate fail, see 'man lockfile-progs' for details
lock_retry=0

lock(){
        lockfile-create -r $lock_retry -p $lock_file && return 0
        echo "ERROR: Can't get lock"
        exit $?
}

unlock(){ lockfile-remove $lock_file; }

mkdir -p "${IMD_RUN}"
lock "${lock_file}" "${lock_retry}"

# give a moment for /dev/disk/by-label/IMD
sleep 1

[ "${mount_source}" ] && {
  [[ -e "${mount_source}" ]] || { unlock ; exit 0 ; }

  mkdir -p "${IMD_MOUNT}"

  mount -o ro "${mount_source}" "${IMD_MOUNT}"
}

[[ -f "${IMD_PATH}" ]] || { unlock ; exit 0 ; }

# xmlstarlet wrapper (namespace, targetfile)
xs () {
  xmlstarlet sel -N 'untrustedhost=https://untrusted.host/xml' "${@}" "${IMD_PATH}"
}

# at this point we have a metadata file, try pulling things from it
xml_hn=$(xs -t -v 'metadata/domain/name')
[[ "${xml_hn}" != '' ]] && { [[ "${run_scripts}" == "true" ]] && hostnamectl set-hostname "${xml_hn}" ; }

mkdir -p /run/untrustedhost/netxml
chmod 0755 /run/untrustedhost
chmod 0755 /run/untrustedhost/netxml
mkdir -p "${systemd_netifdir}"
chmod 0755 "${systemd_netifdir}"
mkdir -p "${interfaces_d_dir}"
chmod 0755 "${interfaces_d_dir}"

router_id=$(xs -t -v metadata/router/@id)

# pull a list of bridges from xml, defined directly or via vlan name assoc.
# NOTE: this will emit spaces in output to break on, currently.
def_bridgelist () {
  xs -t \
   -v 'metadata/domain/devices/interface/source/@bridge' \
   -o ' ' \
   -v 'metadata/domain/metadata/untrustedhost:vlan/bridge/@vlan'
}

# xml path selector string - direct bridge
bp () {
  local b="${1}"
  printf 'metadata/domain/devices/interface[source/@bridge="%s"]' "${b}"
}

# xml path selector string - vlan metadata map
vp () {
  local b="${1}"
  printf 'metadata/domain/metadata/untrustedhost:vlan/bridge[@vlan="%s"]' "${b}"
}

# xml path selector string - vlan metadata by mac
mvp () {
  local m="${1}"
  printf 'metadata/domain/metadata/untrustedhost:vlan/bridge[@mac="%s"]/@vlan' "${m}"
}

# xml path selector string - bridge by mac
mbp () {
  local m="${1}"
  printf 'metadata/domain/devices/interface[mac/@address="%s"]' "${m}"
}

# handle pieces needed for matching a bridge by MAC address and return that
bridge2mac () {
  local b="${1}"
  local mac=''
  mac="$(xs -t -v "$(bp "${b}")/mac/@address")"
  [[ "${mac}" ]] || mac="$(xs -t -v "$(vp "$b")"/@mac)"
  printf '%s' "${mac}"
}

# handle deriving a bridge name from a mac address, allowing our vlan tag stuff to take priority
mac2bridge() {
  local mac="${1}"
  local b=''
  b="$(xs -t -v "$(mvp "${mac}")")"
  [[ "${b}" ]] || b="$(xs -t -v "$(mbp "${mac}")/source/@bridge")"
  printf '%s' "${b}"
}

# check for a boolean value and return appropriately
check_bool () {
  local input="${1}"
  local default="${2}"
  case "${input}" in
    # HACK
    DHCP|dhcp) return "${default}" ;;
    T*|t*|Y*|y*|E*|e*|1) return 0 ;;
    F*|f*|N*|n*|D*|d*|0) return 1 ;;
    *) return "${default}" ;;
  esac
}

# find an mtu if applicable
find_mtu () {
  local bridge="${1}"
  local mac="${2}"
  local mtu=''
  mtu="$(xs -t -v "$(bp "${bridge}")/mtu/@size")"
  [[ "${mtu}" ]] || mtu="$(xs -t -v "$(mbp "${mac}")/mtu/@size")"
  printf '%s' "${mtu}"
}

# find vlans on a base interface
find_base_vlans () {
  local bridge="${1}"
  local mac="${2}"
  local vlans=''
  vlans="$(xs -t -v "$(bp "${bridge}")/vlan/@name")"
  printf '%s' "${vlans}"
}

# find ucarp on a base interface
find_base_ucarp () {
  local bridge="${1}"
  local mac="${2}"
  local ucarps=''
  ucarps="$(xs -t -v "$(bp "${bridge}")/carp4/@address")"
  [[ "${ucarps}" ]] || ucarps="$(xs -t -v "$(mbp "${mac}")/carp4/@address")"
  printf '%s' "${ucarps}"
}

# find ipv4 addresse(s) for a base interface
find_base_ipv4 () {
  local bridge="${1}"
  local mac="${2}"
  local opt="${3}"
  local addrs=''
  local vlans=''
  local ucarps=''
  # check both bridge-y paths for ipv4 addresses...
  addrs="$(xs -t -v "$(bp "${bridge}")/ipv4/@address")"
  [[ "${addrs}" ]] || addrs="$(xs -t -v "$(mbp "${mac}")/ipv4/@address")"
  # if we don't have any explicit ipv4, but we have vlans, disable ipv4.
  vlans="$(find_base_vlans "${bridge}" "${mac}")"
  [[ "${vlans}" ]] && { [[ "${addrs}" ]] || addrs='DISABLED' ; }
  # if we have any ucarp devices, return UNMANAGED
  [[ "${ucarps}" ]] && { [[ "${opt:-}" != "-f" ]] && addrs="UNMANAGED" ; }
  printf '%s' "${addrs}"
}

# find port cost for an interface
find_port_cost () {
  local bridge="${1}"
  local mac="${2}"
  local port_cost=''
  port_cost="$(xs -t -v "$(bp "${bridge}")/bridge/@port_cost")"
  printf '%s' "${port_cost}"
}

find_base_bridge () {
  local bridge="${1}"
  local mac="${2}"
  local destbridge=''
  destbridge="$(xs -t -v "$(bp "${bridge}")/bridge/@name")"
  printf '%s' "${destbridge}"
}

find_base_aggr () {
  local bridge="${1}"
  local mac="${2}"
  local destaggr=''
  destaggr="$(xs -t -v "$(bp "${bridge}")/trunk/@name")"
  printf '%s' "${destaggr}"
}

find_base_onlinereq () {
  local bridge="${1}"
  local mac="${2}"
  local destreq='' ; local ret=''
  destreq="$(xs -t -v "$(bp "${bridge}")/@required_for_online")"
  check_bool "${destreq}" 0 ; ret=$?
  return "${ret}"
}

find_base_sourcetype () {
  local bridge="${1}"
  local mac="${2}"
  local desttype=''
  desttype="$(xs -t -v "$(bp "${bridge}")/source/@type")"
  printf '%s' "${desttype}"
}

find_base_vethpeer () {
  local bridge="${1}"
  local mac="${2}"
  local peer=''
  peer="$(xs -t -v "$(bp "${bridge}")/peer/@name")"
  printf '%s' "${peer}"
}

find_base_vfilter () {
  local bridge="${1}"
  local mac="${2}"
  local ret=''
  filter_attr="$(xs -t -v "$(bp "${bridge}")/bridge/@vlanfilter")"
  # default is to not use vlan filtering
  check_bool "${filter_attr}" 0 ; ret=$?
  return "${ret}"
}

find_base_lacprate () {
  local bridge="${1}"
  local mac="${2}"
  local lacprate=''
  lacprate="$(xs -t -v "$(bp "${bridge}")/trunk/@lacp_xmit_rate")"
  [[ "${lacprate}" ]] || lacprate=slow
  printf '%s' "${lacprate}"
}

find_base_hashpol () {
  local bridge="${1}"
  local mac="${2}"
  local hashpol=''
  hashpol="$(xs -t -v "$(bp "${bridge}")/trunk/@hashpol")"
  [[ "${hashpol}" ]] || hashpol='layer3+4'
  printf '%s' "${hashpol}"
}

find_vid () {
  local tag="${1}"
  local vid=''
  local q=''
  # we're pulling from the ride-along vlan map here
  q="$(printf 'metadata/vlan/map[@name="%s"]/@id' "${tag}")"
  vid="$(xs -t -v "${q}")"
  printf '%s' "${vid}"
}


find_vlan_bridge () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local bn=''
  local vlq=''
  vlq="$(printf 'vlan[@name="%s"]/bridge/@name' "${vlan}")"
  bn="$(xs -t -v "$(bp "${bridge}")/${vlq}")"
  [[ "${bn}" ]] || bn="$(xs -t -v "$(mbp "${mac}")/${vlq}")"
  [[ "${bn}" ]] && return 0
  return 1
}

find_vlan_ipv4 () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local ipv4=''
  local vlq=''
  local rc=''
  vlq="$(printf 'vlan[@name="%s"]/ipv4/@address' "${vlan}")"
  ipv4="$(xs -t -v "$(bp "${bridge}")/${vlq}")"
  [[ "${ipv4}" ]] || ipv4="$(xs -t -v "$(mbp "${mac}")/${vlq}")"
  check_bool "${ipv4}" 2 ; rc=$?
  [[ "${rc}" -ne 2 ]] && return "${rc}"
  printf '%s' "${ipv4}"
}

find_base_mtu () {
  local bridge="${1}"
  local mac="${2}"
  local mtu=''
  mtu="$(xs -t -v "$(bp "${bridge}")/mtu/@size")"
  [[ "${mtu}" ]] || mtu="$(xs -t -v "$(mbp "${mac}")/mtu/@size")"
  printf '%s' "${mtu}"
}

find_vlan_mtu () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local mtu=''
  local vlq=''
  vlq="$(printf 'vlan[@name="%s"]/mtu/@size' "${vlan}")"
  mtu="$(xs -t -v "$(bp "${bridge}")/${vlq}")"
  [[ "${mtu}" ]] || mtu="$(xs -t -v "$(mbp "${mac}")/${vlq}")"
  printf '%s' "${mtu}"
}

find_vlan_rtcount () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local count=''
  local vlq=''
  vlq="$(printf 'vlan[@name="%s"]/ipv4/route' "${vlan}")"
  count="$(xs -t -v "count($(bp "${bridge}")/${vlq})")"
  [[ "${count}" -ne 0 ]] || count="$(xs -t -v "count($(bp "${bridge}")/${vlq})")"
  printf '%s' "${count}"
}

find_vlan_mvl_rtcount () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local target="${4}"
  local count=''
  local vlq=''
  vlq="$(printf 'vlan[@name="%s"]/macvlan[@name="%s"]/ipv4/route' "${vlan}" "${target}")"
  count="$(xs -t -v "count($(bp "${bridge}")/${vlq})")"
  [[ "${count}" -ne 0 ]] || count="$(xs -t -v "count($(bp "${bridge}")/${vlq})")"
  printf '%s' "${count}"
}

find_vlan_rtent_dest () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local idx="${4}"
  local res=''
  local vlq=''
  vlq="$(printf 'vlan[@name="%s"]/ipv4/route[%s]/@destination' "${vlan}" "${idx}")"
  res="$(xs -t -v "$(bp "${bridge}")/${vlq}")"
  [[ "${res}" ]] || res="$(xs -t -v "$(mbp "${mac}")/${vlq}")"
  printf '%s' "${res}"
}

find_vlan_mvl_rtent_dest () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local label="${4}"
  local idx="${5}"
  local res=''
  local vlq=''
  vlq="$(printf 'vlan[@name="%s"]/macvlan[@name="%s"]/ipv4/route[%s]/@destination' "${vlan}" "${label}" "${idx}")"
  res="$(xs -t -v "$(bp "${bridge}")/${vlq}")"
  [[ "${res}" ]] || res="$(xs -t -v "$(mbp "${mac}")/${vlq}")"
  printf '%s' "${res}"
}

find_vlan_targetbridge () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local target=''
  local vlq=''
  vlq="$(printf 'vlan[@name="%s"]/bridge/@name' "${vlan}")"
  target="$(xs -t -v "$(bp "${bridge}")/${vlq}")"
  [[ "${target}" ]] || mtu="$(xs -t -v "$(mbp "${mac}")/${vlq}")"
  printf '%s' "${target}"
}

find_vlan_bridge_stp () {
  # this pulls stp status of THE PARENT BRIDGE
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local target="${4}"
  local res=''
  local rc=''
  res="$(xs -t -v "$(bp "${bridge}")"/bridge/@stp)"
  [[ "${res}" ]] || res="$(xs -t -v "$(mbp "${mac}")/bridge/@stp)")"
  check_bool "${res}" 0 ; rc=$?
  return "${rc}"
}

find_vlan_bridge_stpprio () {
  # this pulls stp priority of the parent bridge interface -or- returns 40k
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local target="${4}"
  local res=''
  res="$(xs -t -v "$(bp "${bridge}")"/bridge/@stp_priority)"
  [[ "${res}" ]] || res="$(xs -t -v "$(mbp "${mac}")/bridge/@stp_priority")"
  [[ "${res}" ]] || res=40000
  printf '%s' "${res}"
}

find_vlan_ucarp () {
  # grab any ucarp ipv4 addresses
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local target="${4}"
  local res=''
  local vlq=''
  vlq="$(printf 'vlan[@name="%s"]/carp4/@address' "${vlan}")"
  res="$(xs -t -v "$(bp "${bridge}")/${vlq}")"
  [[ "${res}" ]] || res="$(xs -t -v "$(mbp "${mac}")/${vlq}")"
  printf '%s' "${res}"
}

find_vlan_macvlan () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local target="${4}"
  local res=''
  local vlq=''
  vlq="$(printf 'vlan[@name="%s"]/macvlan/@name' "${vlan}")"
  res="$(xs -t -v "$(bp "${bridge}")/${vlq}")"
  [[ "${res}" ]] || res="$(xs -t -v "$(mbp "${mac}")/${vlq}")"
  printf '%s' "${res}"
}

find_vlan_mvl_ipv4 () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local target="${4}"
  local ipv4=''
  local vlq=''
  local rc=''
  vlq="$(printf 'vlan[@name="%s"]/macvlan[@name="%s"]/ipv4/@address' "${vlan}" "${target}")"
  ipv4="$(xs -t -v "$(bp "${bridge}")/${vlq}")"
  [[ "${ipv4}" ]] || ipv4="$(xs -t -v "$(mbp "${mac}")/${vlq}")"
  check_bool "${ipv4}" 2 ; rc=$?
  [[ "${rc}" -ne 2 ]] && return "${rc}"
  printf '%s' "${ipv4}"
}

find_vlan_encap () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local vlq=''
  local res=''
  local rc=''
  vlq="$(printf 'vlan[@name="%s"]/encapsulation/@type' "${vlan}")"
  res="$(xs -t -v "$(bp "${bridge}")/${vlq}")"
  [[ "${res}" ]] || res="$(xs -t -v "$(mbp "${mac}")/${vlq}")"
  check_bool "${res}" 2 ; rc=$?
  [[ "${rc}" -ne 2 ]] && return "${rc}"
  case "${res}" in
    untagged|UNTAGGED) return 1 ;;
    *)                 return 0 ;;
  esac
}

find_vlan_fwzone () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local vlq=''
  local res=''
  vlq="$(printf 'vlan[@name="%s"]/firewall/@zone' "${vlan}")"
  res="$(xs -t -v "$(bp "${bridge}")/${vlq}")"
  [[ "${res}" ]] || res="$(xs -t -v "$(mbp "${mac}")/${vlq}")"
  printf '%s' "${res}"
}

find_vlan_mvl_fwzone () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local target="${4}"
  local vlq=''
  local res=''
  vlq="$(printf 'vlan[@name="%s"]/macvlan[@name="%s"]/firewall/@zone' "${vlan}" "${target}")"
  res="$(xs -t -v "$(bp "${bridge}")/${vlq}")"
  [[ "${res}" ]] || res="$(xs -t -v "$(mbp "${mac}")/${vlq}")"
  printf '%s' "${res}"
}

print_lldpconf () {
  [[ "${systemd_lldp}" == "true" ]] || return 0
  printf '%s=yes\n' 'LLDP' 'EmitLLDP'
}

print_ipv4conf () {
  local ipaddr="${1}"
  local usemtu="${2}"
  local ent=''
  [[ "${usemtu}" ]] || usemtu="${systemd_usemtu}"
  case "${ipaddr}" in
    *DHCP*|*dhcp*)
      print_lldpconf
      printf 'DHCP=%s\n' 'yes'
      [[ "${usemtu}" == "true" ]] && {
        printf '[%s]\n' 'DHCPv4'
        printf 'UseMTU=%s\n' 'true'
      }
    ;;
    */*)
      print_lldpconf
      for ent in ${ipaddr} ; do
        printf 'Address=%s\n' "${ent}"
      done
      printf '%s=yes\n' 'ConfigureWithoutCarrier' 'IgnoreCarrierLoss'
    ;;
    *disable*|*DISABLE*)
      printf '%s=no\n' 'LinkLocalAddressing' 'LLMNR' 'IPv6AcceptRA'
      printf '%s=yes\n' 'ConfigureWithoutCarrier' 'IgnoreCarrierLoss'
    ;;
  esac
}

# process link renaming/mtu
proc_linknaming () {
  local bridge="${1}"
  local mac="${2}"
  local mtu=''
  mtu="$(find_mtu "${bridge}" "${mac}")"

  # we only generate a link file if we get a mac to rename...
  [[ "${mac}" ]] &&  {
    printf '%s\n' '[Match]'
    printf 'MACAddress=%s\n' "${mac}"
    printf '%s\n' '[Link]'
    [[ "${mtu}" ]] && printf 'MTUBytes=%s\n' "${mtu}"
    printf 'Name=%s\n' "${bridge}"
  } > "${systemd_netifdir}/${bridge}.link"

  # HACK. rename the interface now if needed.
  local f='' ifmac='' ext_ifname=''
  for f in /sys/class/net/*/address ; do
    read -r ifmac < "${f}"
    [[ "${ifmac}" == "${int_mac}" ]] && {
      ext_ifname="${f%/address}"
      ext_ifname="${ext_ifname##*/}"
      # if it's already the name, evaporate into noop
      [[ "${ext_ifname}" == "${bridge}" ]] && ext_ifname=''
    }
  done
  [[ "${ext_ifname}" ]] && {
    /usr/bin/ip link set dev "${ext_ifname}" down
    /usr/bin/ip link set dev "${ext_ifname}" name "${bridge}"
    /usr/bin/ip link set dev "${bridge}" up
  }

  # ditto MTU!
  [[ -e "/sys/class/net/${bridge}/mtu" ]] && {
    local ifmtu=''
    read -r ifmtu < "/sys/class/net/${bridge}/mtu"
    [[ "${mtu}" ]] && [[ "${mtu}" != "${ifmtu}" ]] && [[ "${run_scripts}" == "true" ]] && {
      /usr/bin/ip link set dev "${bridge}" mtu "${mtu}"
    }
  }
}

proc_vlan_targetbridge () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local target="${4}"
  [[ "${target}" ]] || return 0
  local ipaddr="${5}"
  local mtu="${6}"
  local ucarp="${7}"
  # we only support bridge stp priority here, so...
  local stp=''
  local stpprio=''
  find_vlan_bridge_stp "${bridge}" "${mac}" "${vlan}" "${target}" ; stp=$?
  stpprio="$(find_vlan_bridge_stpprio "${bridge}" "${mac}" "${vlan}" "${target}")"
  [[ "${ucarp}" ]] && ipaddr='DISABLE'
  # we always create the bridge netdev if targeted. no real harm.
  {
    printf '[%s]\n' 'NetDev'
    printf 'Name=%s\n' "${target}"
    printf 'Kind=%s\n' 'bridge'
    [[ "${mtu}" ]] && printf 'MTUBytes=%s\n' "${mtu}"
    printf '%s\n' '[Bridge]'
    [[ "${stp}" -eq 0 ]] && printf 'STP=%s\n' "yes"
    [[ "${stp}" -eq 1 ]] && printf 'STP=%s\n' "no"
    printf 'Priority=%s\n' "${stpprio}"
  } > "${systemd_netifdir}/${target}.netdev"
  # we only make a network file if it didn't exist before, though.
  [[ ! -e "${systemd_netifdir}/${target}.network" ]] && {
    printf '[%s]\n' 'Match'
    printf 'Name=%s\n' "${target}"
    printf '[%s]\n' 'Link'
    [[ "${mtu}" ]] && printf 'MTUBytes=%s\n' "${mtu}"
    printf '[%s]\n' 'Network'
    printf 'ConfigureWithoutCarrier=%s\n' 'yes'
    printf 'IgnoreCarrierLoss=%s\n' 'yes'
  } > "${systemd_netifdir}/${target}.network"
  # handle IP addressing, or an explicit disable
  {
    print_ipv4conf "${ipaddr}"
  } >> "${systemd_netifdir}/${target}.network"
}

proc_vlan_ucarp () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local targetbridge="${4}"
  local ipaddr="${5}"
  local mtu="${6}"
  local ucarp="${7}"
  local static_rt_count='' ; local static_rt_dest=''
  [[ "${ucarp}" ]] || return 0
  # pull the _rest_ of the ucarp settings now
  local attr=''
  vlq="$(printf 'vlan[@name="%s"]/carp4' "${vlan}")"
  declare -A ucarp_attrs
  for attr in pass vhid advbase advskew master script_ipup script_ipdown ; do
    ucarp_attrs[${attr}]="$(xs -t -v "$(bp "${bridge}")/${vlq}/@${attr}")"
    [[ "${ucarp_attrs[${attr}]}" ]] || ucarp_attrs[${attr}]="$(xs -t -v "$(mbp "${mac}")/${vlq}/@${attr}")"
  done
  static_rt_count="$(find_vlan_rtcount "${bridge}" "${mac}" "${vlan}")"
  {
    printf 'auto %s\niface %s inet static\n' "${bridge}-vl-${vlan}" "${bridge}-vl-${vlan}"
    printf '  address %s\n' "${ipaddr}"
    printf '  # UCARP config\n  ucarp-vip %s\n' "${ucarp%/*}"
    printf '  ucarp-vid %s\n' "${ucarp_attrs[vhid]}"
    printf '  ucarp-password %s\n' "${ucarp_attrs[pass]}"
    printf '  ucarp-advskew %s\n' "${ucarp_attrs[advskew]}"
    printf '  ucarp-advbase %s\n' "${ucarp_attrs[advbase]}"
    printf '  ucarp-master %s\n' "${ucarp_attrs[master]:-no}"
	  [[ "${ucarp_attrs[script_ipup]}" ]] && printf '  ucarp-upscript %s\n' "${ucarp_attrs[script_ipup]}"
	  [[ "${ucarp_attrs[script_ipdown]}" ]] && printf '  ucarp-downscript %s\n' "${ucarp_attrs[script_ipdown]}"
    while [[ "${static_rt_count}" -gt 0 ]] ; do
      static_rt_dest="$(find_vlan_rtent_dest "${bridge}" "${mac}" "${vlan}" "${static_rt_count}")"
      printf '  post-up ip route add %s dev %s\n' "${static_rt_dest}" "${bridge}-vl-${vlan}"
      ((static_rt_count--))
    done
    printf 'iface %s:ucarp inet static\n' "${bridge}-vl-${vlan}"
    printf '  address %s\n' "${ucarp}"
  } > "${interfaces_d_dir}/${bridge}-vl-${vlan}"
  # doing the below currently breaks systemd-networkd config on raspios entirely...?
  # {
  #   printf '[Link]\nUnmanaged=%s\n' "yes"
  # } > "/run/systemd/network/${vl_target}-vl-${vl_name}.network"
}

proc_vlan_macvlan () {
  local bridge="${1}"
  local mac="${2}"
  local vlan="${3}"
  local target="${4}"
  local ipaddr="${5}"
  local mtu="${6}"
  local ucarp="${7}"
  local vl_macvlan="${8}"
  local vl_mvl_label=''
  local vl_mvl_ipv4=''
  local vl_mvl_addr=''
  local vl_mvl_fwz=''
  local vl_mvl_rtc=''
  local vl_mvl_rtdest=''
  [[ "${vl_macvlan}" ]] || return 0
  # walk the strings in vl_macvlan and wire those interfaces.
  for vl_mvl_label in ${vl_macvlan} ; do
    # find the bridge settings now
    vl_mvl_ipv4="$(find_vlan_mvl_ipv4 "${bridge}" "${mac}" "${vlan}" "${vl_mvl_label}")"
    vl_mvl_fwz="$(find_vlan_mvl_fwzone "${bridge}" "${mac}" "${vlan}" "${vl_mvl_label}")"
    vl_mvl_rtc="$(find_vlan_mvl_rtcount "${bridge}" "${mac}" "${vlan}" "${vl_mvl_label}")"
    {
      printf '[%s]\n' 'NetDev'
      printf 'Name=%s\n' "${vl_mvl_label}"
      printf 'Kind=%s\n' 'macvlan'
      printf '[%s]\n' 'MACVLAN'
      printf 'Mode=%s\n' 'bridge'
    } > "/run/systemd/network/${vl_mvl_label}.netdev"
    {
      printf '[%s]\n' 'Match'
      printf 'Name=%s\n' "${vl_mvl_label}"
      printf '[%s]\n' 'Network'
      [[ "${vl_mvl_ipv4}" ]] && {
        for vl_mvl_addr in ${vl_mvl_ipv4} ; do
          printf 'Address=%s\n' "${vl_mvl_addr}"
        done
      }
      [[ "${vl_mvl_rtc}" -gt 0 ]] && {
        printf '[%s]\n' 'Route'
        while [[ "${vl_mvl_rtc}" -gt 0 ]] ; do
          vl_mvl_rtdest="$(find_vlan_mvl_rtent_dest "${bridge}" "${mac}" "${vlan}" "${vl_mvl_label}" "${vl_mvl_rtc}")"
          printf 'Destination=%s\n' "${vl_mvl_rtdest}"
          ((vl_mvl_rtc--))
        done
      }
    } > "/run/systemd/network/${vl_mvl_label}.network"
    [[ "${vl_mvl_fwz}" ]] && {
      type firewall-cmd 2>/dev/null 1>&2 && firewall-cmd --zone="${vl_mvl_fwz}" --change-interface="${vl_mvl_label}"
    }
  done
}

proc_vlan_fwzone () {
  local bridge="${1}"
  # shellcheck disable=SC2034
  local mac="${2}"
  local vlan="${3}"
  local fwzone="${4}"
  type firewall-cmd 2>/dev/null 1>&2 || return 1
  firewall-cmd --zone="${fwzone}" --change-interface="${bridge}-vl-${vlan}"
}

# process vlan devices and network settings
proc_vlans () {
  local bridge="${1}"
  local mac="${2}"
  local vlans=''
  local vlan=''
  local destbridge=''
  local vl_filt=''
  local vid=''
  local vl_ip=''
  local vl_mtu=''
  local vl_tgtbridge=''
  local vl_ucarp=''
  local vl_macvlan=''
  local vl_mvl_label=''
  local vl_fwzone=''
  local vl_rtcount=''
  local vl_rtent_dest=''
  find_base_vfilter "${bridge}" "${mac}" ; vl_filt=$?
  destbridge="$(find_base_bridge "${bridge}" "${mac}")"
  [[ "${destbridge}" ]] || destbridge="${bridge}"
  vlans="$(find_base_vlans "${bridge}" "${mac}")"
  for vlan in ${vlans} ; do
    vl_ip=$(find_vlan_ipv4 "${bridge}" "${mac}" "${vlan}")
    # if we are using VLAN filtering on bridges, we only need the vlan devce for ip addressing.
    vl_tgtbridge="$(find_vlan_targetbridge "${bridge}" "${mac}" "${vlan}")"
    [[ "${vl_ip}" == '' ]] && [[ "${vl_tgtbridge}" == '' ]] && [[ "${vl_filt}" -ne 1 ]] && continue
    # if we are assigning an IP here, check for a firewalld zone and glue that in
    [[ "${vl_tgtbridge}" == '' ]] && vl_fwzone="$(find_vlan_fwzone "${bridge}" "${mac}" "${vlan}")"
    [[ "${vl_fwzone}" ]] && proc_vlan_fwzone "${bridge}" "${mac}" "${vlan}" "${vl_fwzone}"
    vl_mtu="$(find_vlan_mtu "${bridge}" "${mac}" "${vlan}")"
    vl_ucarp="$(find_vlan_ucarp "${bridge}" "${mac}" "${vlan}")"
    vl_macvlan="$(find_vlan_macvlan "${bridge}" "${mac}" "${vlan}")"
    vid="$(find_vid "${vlan}")"
    {
      printf '%s\n' '[NetDev]'
      printf 'Name=%s-vl-%s\n' "${destbridge}" "${vlan}"
      printf 'Kind=%s\n' 'vlan'
      [[ "${vl_mtu}" ]] && printf 'MTUBytes=%s\n' "${vl_mtu}"
      printf '%s\n' '[VLAN]'
      printf 'Id=%s\n' "${vid}"
    } > "${systemd_netifdir}/${destbridge}-vl-${vlan}.netdev"
    # if we call proc_vlan_ucarp with no vl_ucarp, it nops out
    proc_vlan_ucarp "${bridge}" "${mac}" "${vlan}" "${vl_tgtbridge}" "${vl_ip}" "${vl_mtu}" "${vl_ucarp}"
    # if we call proc_vlan_targetbridge with no targetbridge, it nops out
    proc_vlan_targetbridge "${bridge}" "${mac}" "${vlan}" "${vl_tgtbridge}" "${vl_ip}" "${vl_mtu}" "${vl_ucarp}"
    proc_vlan_macvlan "${bridge}" "${mac}" "${vlan}" "${vl_tgtbridge}" "${vl_ip}" "${vl_mtu}" "${vl_ucarp}" "${vl_macvlan}"
    [[ "${vl_ucarp}" ]] && vl_ip='DISABLED'
    [[ "${vl_tgtbridge}" ]] && vl_ip='DISABLED'
    {
      printf '[%s]\n' 'Match'
      printf 'Name=%s\n' "${destbridge}-vl-${vlan}"
      printf '[%s]\n' 'Link'
      [[ "${vl_mtu}" ]] && printf 'MTUBytes=%s\n' "${vl_mtu}"
      printf '[%s]\n' 'Network'
      print_ipv4conf "${vl_ip}"
      [[ "${vl_tgtbridge}" ]] && printf 'Bridge=%s\n' "${vl_tgtbridge}"
      [[ "${vl_macvlan}" ]] && {
        for vl_mvl_label in ${vl_macvlan} ; do
          printf 'MACVLAN=%s\n' "${vl_mvl_label}"
        done
      }
    } > "${systemd_netifdir}/${destbridge}-vl-${vlan}.network"
    # inserting static routes kludge
    vl_rtcount="$(find_vlan_rtcount "${bridge}" "${mac}" "${vlan}")"
    # well...unless you're in ucarp, then hahaha
    [[ "${vl_ip}" == "DISABLED" ]] && vl_rtcount=0
    [[ "${vl_rtcount}" -gt 0 ]] && {
      printf '[%s]\n' 'Route' >> "${systemd_netifdir}/${destbridge}-vl-${vlan}.network"
    }
    while [[ "${vl_rtcount}" -gt 0 ]] ; do
      vl_rtent_dest="$(find_vlan_rtent_dest "${bridge}" "${mac}" "${vlan}" "${vl_rtcount}")"
      printf 'Destination=%s\n' "${vl_rtent_dest}"
      ((vl_rtcount--))
    done
  done
}

# process aggregated links (trunks)
proc_aggrs () {
  local bridge="${1}"
  local mac="${2}"
  local cost="${3}"
  local mtu=''
  local destaggr=''
  local xrate=''
  local xmithash=''
  local vl_filt=''
  local destbridge=''
  destaggr="$(find_base_aggr "${bridge}" "${mac}")"
  [[ "${destaggr}" ]] || return 0
  local lcost=''
  lcost="$(find_port_cost "${bridge}" "${mac}")"
  [[ "${lcost}" ]] && cost="${lcost}"
  find_base_vfilter "${bridge}" "${mac}" ; vl_filt=$?
  destbridge="$(find_base_bridge "${bridge}" "${mac}")"
  mtu="$(find_base_mtu "${bridge}" "${mac}")"
  xrate="$(find_base_lacprate "${bridge}" "${mac}")"
  xmithash="$(find_base_hashpol "${bridge}" "${mac}")"
  {
    printf '[%s]\n' 'NetDev'
    printf 'Name=%s\n' "${destaggr}"
    printf 'Kind=%s\n' 'bond'
    [[ "${mtu}" ]] && printf 'MTUBytes=%s\n' "${mtu}"
    printf '[%s]\n' 'Bond'
    printf 'Mode=%s\n' '802.3ad'
    printf 'MIIMonitorSec=%s\n' '1s'
    printf 'UpDelaySec=%s\n' '2s'
    printf 'DownDelaySec=%s\n' '8s'
    printf 'LACPTransmitRate=%s\n' "${xrate}"
    printf 'TransmitHashPolicy=%s\n' "${xmithash}"
  } > "${systemd_netifdir}/${destaggr}.netdev"
  {
    printf '[%s]\n' 'Match'
    printf 'Name=%s\n' "${destaggr}"
    printf '[%s]\n' 'Link'
    [[ "${mtu}" ]] && printf 'MTUBytes=%s\n' "${mtu}"
    printf '[%s]\n' 'Network'
    [[ "${destbridge}" ]] && {
      printf 'Bridge=%s\n' "${destbridge}"
      printf '[%s]\n' 'Bridge'
      printf 'Cost=%s\n' "${cost}"
      [[ "${vl_filt}" -eq 0 ]] && {
        vlans="$(find_base_vlans "${bridge}" "${mac}")"
        # process vlan filtering
        for vlan in ${vlans} ; do
          vid="$(find_vid "${vlan}")"
          printf '[%s]\n' 'BridgeVLAN'
          printf 'VLAN=%s\n' "${vid}"
          find_vlan_encap "${bridge}" "${mac}" "${vlan}" ; encap=$?
          [[ "${encap}" -eq 1 ]] && {
            printf 'EgressUntagged=%s\n' "${vid}"
            printf 'PVID=%s\n' "${vid}"
          }
        done
      }
    }
  } > "${systemd_netifdir}/${destaggr}.network"
}

# process bridges that interfaces bind to
proc_bridges () {
  local bridge="${1}"
  local mac="${2}"
  local destbridge=''
  local mtu=''
  local vl_filt=''
  local stp=''
  local stpprio=''
  local vlans=''
  local vlan=''
  local vid=''
  local vlan_ipv4=''
  local bridge_ipv4=''
  destbridge="$(find_base_bridge "${bridge}" "${mac}")"
  [[ "${destbridge}" ]] || return 0
  mtu="$(find_base_mtu "${bridge}" "${mac}")"
  find_base_vfilter "${bridge}" "${mac}" ; vl_filt=$?
  find_vlan_bridge_stp "${bridge}" "${mac}" ; stp=$?
  stpprio="$(find_vlan_bridge_stpprio "${bridge}" "${mac}")"
  # we may need vlans if we are a filtering bridge or bridge-on-vlan setup
  vlans="$(find_base_vlans "${bridge}" "${mac}")"
  # we will copy over the base interface ipv4 config if have an explicit one.
  bridge_ipv4="$(find_base_ipv4 "${bridge}" "${mac}")"
  {
    printf '[%s]\n' 'NetDev'
    printf 'Name=%s\n' "${destbridge}"
    printf 'Kind=%s\n' 'bridge'
    [[ "${mtu}" ]] && printf 'MTUBytes=%s\n' "${mtu}"
    printf '[%s]\n' 'Bridge'
    [[ "${stp}" -eq 0 ]] && printf 'STP=%s\n' 'yes'
    [[ "${stp}" -ne 0 ]] && printf 'STP=%s\n' 'no'
    printf 'Priority=%s\n' "${stpprio}"
    [[ "${vl_filt}" -eq 0 ]] && {
      printf 'DefaultPVID=%s\n' 'none'
      printf 'VLANFiltering=%s\n' 'yes'
    }
    printf 'MulticastQuerier=%s\n' 'yes'
    printf 'MulticastSnooping=%s\n' 'no'
    printf 'ForwardDelaySec=%s\n' '2'
  } > "${systemd_netifdir}/${destbridge}.netdev"
  {
    printf '[%s]\n' 'Match'
    printf 'Name=%s\n' "${destbridge}"
    printf '[%s]\n' 'Network'
    # VLANs on bridge with IP addresses?
    for vlan in ${vlans} ; do
      find_vlan_ipv4 "${bridge}" "${mac}" "${vlan}" >/dev/null ; vlan_ipv4=$?
      [[ "${vlan_ipv4}" -ne 1 ]] && {
        printf 'VLAN=%s\n' "${destbridge}-vl-${vlan}"
        continue
      }
      find_vlan_bridge "${bridge}" "${mac}" "${vlan}" >/dev/null && {
        printf 'VLAN=%s\n' "${destbridge}-vl-${vlan}"
      }
    done
    # IPv4 directly on bridge?
    [[ "${bridge_ipv4}" ]] && {
      case "${bridge_ipv4}" in
        disabled) printf '%s=no\n' 'LinkLocalAddressing' 'IPv6AcceptRA' ;;
        *) print_ipv4conf "${bridge_ipv4}" "false" ;; # we're not gonna take the MTU here for the bridge.
      esac
    }
    printf '[%s]\n' 'Link'
    # oh, ffs systemd-networkd
    [[ "${ipv4}" == "disabled" ]] && printf 'RequiredForOnline=%s\n' 'carrier'
    [[ "${mtu}" ]] && printf 'MTUBytes=%s\n' "${mtu}"
    # VLAN filtering requires we specify VLANs usable _on the bridge_
    [[ "${vl_filt}" -eq 0 ]] && {
      for vlan in ${vlans} ; do
        vid="$(find_vid "${vlan}")"
        printf '[%s]\n' 'BridgeVLAN'
        printf 'VLAN=%s\n' "${vid}"
      done
    }
  } > "${systemd_netifdir}/${destbridge}.network"
}

# process basic interface config
proc_interface () {
  local bridge="${1}"
  local mac="${2}"
  local cost="${3}"
  [[ "${bridge}${mac}" ]] || return 0
  local vlans=''
  local vl_filt=''
  local ipv4=''
  local mtu=''
  local destbridge=''
  local vlan=''
  local vid=''
  local encap=''
  local destaggr=''
  local lcost=''
  lcost="$(find_port_cost "${bridge}" "${mac}")"
  [[ "${lcost}" ]] && cost="${lcost}"
  mtu="$(find_base_mtu "${bridge}" "${mac}")"
  ipv4="$(find_base_ipv4 "${bridge}" "${mac}")"
  find_base_vfilter "${bridge}" "${mac}" ; vl_filt=$?
  destbridge="$(find_base_bridge "${bridge}" "${mac}")"
  destaggr="$(find_base_aggr "${bridge}" "${mac}")"
  {
    printf '[%s]\n' 'Match'
    [[ "${mac}" ]] && printf 'MACAddress=%s\n' "${mac}"
    [[ "${mac}" ]] || { [[ "${bridge}" ]] && printf 'Name=%s\n' "${bridge}" ; }
    printf '[%s]\n' 'Link'
    [[ "${mtu}" ]] && printf 'MTUBytes=%s\n' "${mtu}"
    find_base_onlinereq "${bridge}" "${mac}" || printf 'RequiredForOnline=%s\n' 'no'
    printf '[%s]\n' 'Network'
    # destination bridges are...weird, since they hijack our config.
    [[ "${destbridge}" ]] || print_ipv4conf "${ipv4}"
    # handle *no* configuration (dhcp the thing)
    [[ "${ipv4}" ]] || [[ "${destaggr}" ]] || [[ "${destbridge}" ]] || [[ "${vlans}" ]] || print_ipv4conf "DHCP"
    [[ "${destaggr}" ]] && {
      printf 'Bond=%s\n' "${destaggr}"
    }
    # if we're plumbing something below a bridge...do this
    if [[ "${destbridge}" ]] ; then
      [[ -z "${destaggr}" ]] && {
        printf 'Bridge=%s\n' "${destbridge}"
        printf '[%s]\n' 'Bridge'
        printf 'Cost=%s\n' "${cost}"
        [[ "${vl_filt}" -eq 0 ]] && {
          vlans="$(find_base_vlans "${bridge}" "${mac}")"
          # process vlan filtering
          for vlan in ${vlans} ; do
            vid="$(find_vid "${vlan}")"
            printf '[%s]\n' 'BridgeVLAN'
            printf 'VLAN=%s\n' "${vid}"
            find_vlan_encap "${bridge}" "${mac}" "${vlan}" ; encap=$?
            [[ "${encap}" -eq 1 ]] && {
              printf 'EgressUntagged=%s\n' "${vid}"
              printf 'PVID=%s\n' "${vid}"
            }
          done
        }
      }
    # case for direcly using VLANs (no bridge)
    else
      [[ "${vl_filt}" -eq 0 ]] && {
        vlans="$(find_base_vlans "${bridge}" "${mac}")"
        # glue.
        for vlan in ${vlans} ; do
          printf 'VLAN=%s\n' "${bridge}-vl-${vlan}"
        done
      }
    fi
  } > "${systemd_netifdir}/${bridge}.network"
  # this is an odd one. if we have a veth pair, create the netdev file for *half* of it.
  local sourcetype=''
  sourcetype="$(find_base_sourcetype "${bridge}" "${mac}")"
  case "${sourcetype}" in
    veth)
      local veth_peer=''
      veth_peer="$(find_base_vethpeer "${bridge}" "${mac}")"
      # veth_peer *must* exist to continue
      [[ "${veth_peer}" ]] && {
        # but we skip making our own netdev file if the peer made one
        [[ -f "${systemd_netifdif}/${veth_peer}.netdev" ]] || {
          printf '[%s]\n' 'NetDev'
          printf 'Name=%s\n' "${bridge}"
          printf 'Kind=%s\n' 'veth'
          printf '[%s]\n' 'Peer'
          printf 'Name=%s\n' "${veth_peer}"
        } > "${systemd_netifdir}/${bridge}.netdev"
      }
    ;;
  esac
}

# initial bridge costing if one is not set. defaults to *expensive* and goes up by one per loop. ;)
base_cost=59999

# turn globbing off to walk the list of bridge interfaces, but back *on* immediately after.
set -o noglob
for b in $(def_bridgelist) ; do
  set +o noglob
  selpath="metadata/domain/devices/interface[source/@bridge=\"${b}\"]"
  base_cost=$((base_cost+1))
  int_mac='' ; ext_ifname='' ; ifmac='' ; ifmtu=''
  int_ucarp4='' ;
  ipaddr='' ; vid=''
  wifi_ssid='' ; wifi_psk='' ; wifi_source=''
  wwan_apn=''

  int_mac="$(bridge2mac "${b}")"
  # if we got a mac address for the bridge, try reversing it (for cases of vlan tagging oddity)
  [[ "${int_mac}" ]] && b="$(mac2bridge "${int_mac}")"

  # handle link renaming, initial mtu
  proc_linknaming "${b}" "${int_mac}"

  # create vlans now
  proc_vlans "${b}" "${int_mac}"

  # create dest linkaggs now
  proc_aggrs "${b}" "${int_mac}" "${base_cost}"

  # create dest bridges now
  proc_bridges "${b}" "${int_mac}"

  # process the actual interface network/device config
  proc_interface "${b}" "${int_mac}" "${base_cost}"

  wifi_ssid="$(xmlstarlet sel -t -v "${selpath}/wifi/@ssid" "${IMD_PATH}")"
  wifi_psk="$(xmlstarlet sel -t -v "${selpath}/wifi/@psk" "${IMD_PATH}")"
  wifi_source="$(xmlstarlet sel -t -v "${selpath}/wifi/@source" "${IMD_PATH}")"
  wwan_apn="$(xmlstarlet sel -t -v "${selpath}/wwan/@apn" "${IMD_PATH}")"
  
  # handle hostapd just before wireless client.
  [[ "${wifi_source}" ]] && {
    [[ ! -e "/sys/class/net/${b}" ]] && {
      [[ -e "/sys/class/net/${wifi_source}/phy80211" ]] && {
	wi_phy='' ; wi_phy="$(basename "$(readlink "/sys/class/net/${wifi_source}/phy80211")")"
	iw phy "${wi_phy}" interface add "${b}" type __ap
	# this is dummmmb
	sleep 1
	ap_cand=() ; ap_cand=(/sys/class/net/*/phy80211)
	for _nd_cand in "${ap_cand[@]}" ; do
          _tgtphy="$(basename "$(readlink "${_nd_cand}")")"
	  [[ "${_tgtphy}" == "${wi_phy}" ]] && {
            _tgtdev="$(basename "$(dirname "${_nd_cand}")")"
            [[ "${_tgtdev}" != "${wifi_source}" ]] && {
              /usr/bin/ip link set dev "${_tgtdev}" down
              /usr/bin/ip link set dev "${_tgtdev}" name "${b}"
            }
          }
        done
      }
    }
  }

  # handle the wifi association if we have one
  [[ "${wifi_ssid}${wifi_psk}" ]] && {
    mkdir -p "/run/untrustedhost/wpa_supplicant"
    {
      printf 'update_config=%s\n' '1'
      printf 'ctrl_interface=%s\n' '/var/run/wpa_supplicant'
      printf 'network={\n'
      [[ "${wifi_ssid}" ]] && printf '  ssid="%s"\n' "${wifi_ssid}"
      [[ "${wifi_psk}" ]]  && printf '  psk="%s"\n'  "${wifi_psk}"
      printf '}\n'
    } > "/run/untrustedhost/wpa_supplicant/${b}.conf"
    systemctl start "untrustedhost-wpasupplicant@${b}.service"
  }

  # wwan support could be...improved. we assume exactly one modem.
  [[ "${wwan_apn}" ]] && {
    mkdir -p /run/untrustedhost/mmcli-simpleconnect
    printf 'apn=%s' "${wwan_apn}" > "/run/untrustedhost/mmcli-simpleconnect/0"
    systemd-run --on-active=30s systemctl start untrustedhost-mmcli-simpleconnect@0
  }
done

# in case there were no bridges.
set +o noglob

[[ "${router_id}" ]] && {
  printf 'router id %s;\n' "${router_id}" > /run/untrustedhost/router_id.conf
  chown bird:bird /run/untrustedhost/router_id.conf

  # check for ospf areas assigned to interfaces
  ospf_areas='' ; ospf_areas="$(xmlstarlet sel -t -v 'metadata/domain/devices/interface[@type="bridge"]/ospf/@area' "${IMD_PATH}")"
  [[ "${ospf_areas[*]}" ]] && {
    rm -f /run/untrustedhost/ospf_areas.conf
    {
      ospf_seen=()
      # write the ospf protocol section
      printf 'protocol ospf {\n  export where match_route();\n'
      for area in ${ospf_areas} ; do
        # write an area section
        case " ${ospf_seen[*]} " in
          *" ${area} "*) continue ;;
        esac
        printf '  area %s {\n    stub no;\n' "${area}"
        # process interfaces for the area
        ospf_macs=''
        # match mac addresses because the name can be in _two_ places
        ospf_macs="$(xmlstarlet sel -t -v 'metadata/domain/devices/interface[@type="bridge"][ospf/@area="'"${area}"'"]/mac/@address' "${IMD_PATH}")"
        for macaddr in ${ospf_macs} ; do
          # first use the untrustedhost space
          ospf_ifn="$(xmlstarlet sel -N 'untrustedhost=https://untrusted.host/xml' -t -v 'metadata/domain/metadata/untrustedhost:vlan/bridge[@mac="'"${macaddr}"'"]/@vlan' "${IMD_PATH}")"
          # else try the bridge name
          [[ "${ospf_ifn}" ]] || ospf_ifn="$(xmlstarlet sel -t -v 'metadata/domain/devices/interface[@type="bridge"][mac/@address="'"${macaddr}"'"]/source/@bridge' "${IMD_PATH}")"
          printf '    interface "%s" {\n' "${ospf_ifn}"
          # now, check for authentications...
          ospf_keyids=''
          ospf_keyids="$(xmlstarlet sel -t -v 'metadata/router/ospf[@area="'"${area}"'"]/authentication/@key' "${IMD_PATH}")"
          [[ "${ospf_keyids[*]}" ]] && printf '      authentication cryptographic;\n'
          for keyid in ${ospf_keyids} ; do
            ospf_pass=''
            ospf_pass="$(xmlstarlet sel -t -v 'metadata/router/ospf[@area="'"${area}"'"]/authentication[@key="'"${keyid}"'"]/@password' "${IMD_PATH}")"
            [[ "${ospf_pass}" ]] && printf '      password "%s" { id %s; };\n' "${ospf_pass}" "${keyid}"
          done
          printf '      type broadcast;\n    };\n'
        done
        printf '  };\n'
        ospf_seen=("${ospf_seen[@]}" "${area}")
      done
      # close the ospf proto section
      printf '}\n'
    } >> /run/untrustedhost/ospf_areas.conf
    chown bird:bird /run/untrustedhost/ospf_areas.conf

    # ask bird to start after us
    systemd-run --on-active=5s systemctl start untrustedhost-bird
  }
}

# ssh keying is handled _here_ rather than extensions in case those fail.
[[ "${run_scripts}" == "true" ]] && {
  ssh_userkey="$(xmlstarlet sel -t -v 'metadata/ssh/pubkey' "${IMD_PATH}")"
  [[ "${ssh_userkey}" ]] && {
    uhome="$(eval echo "~${ssh_user}")"
    mkdir -p "${uhome}/.ssh"
    printf '%s\n' "${ssh_userkey}" > "${uhome}/.ssh/authorized_keys"
    chown -R "${ssh_user}:${ssh_user}" "${uhome}/.ssh"
    chmod 0700 "${uhome}/.ssh"
    chmod 0600 "${uhome}/.ssh/authorized_keys"
  }
  # TODO: ssh host key handling(?)

  for f in /usr/lib/untrustedhost/imd/* ; do
    [[ -x "${f}" ]] && { "${f}" || { { [ "${mount_source}" ] && umount "${mount_dest}" ; } ; unlock ; exit 1 ; } ; }
  done
}
[[ "${run_scripts}" == "true" ]] || true

{ [ "${mount_source}" ] && umount "${mount_dest}" ; } || true

unlock
