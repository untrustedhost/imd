#!/usr/bin/env bash

# we expect to be 'in' the src/ directory
set -eux

# shell libraries
install --mode=0755 --owner=0 --group=0 --directory /usr/lib/untrustedhost/shelllib
install --mode=0644 --owner=0 --group=0 ./shelllib/core.bash /usr/lib/untrustedhost/shelllib/core.bash
install --mode=0644 --owner=0 --group=0 ./shelllib/imd.bash /usr/lib/untrustedhost/shelllib/imd.bash
install --mode=0644 --owner=0 --group=0 ./shelllib/service-check.bash /usr/lib/untrustedhost/shelllib/service-check.bash

# scripts
install --mode=0755 --owner=0 --group=0 --directory /usr/lib/untrustedhost/scripts
install --mode=0755 --owner=0 --group=0 ./imd.sh /usr/lib/untrustedhost/scripts/imd.sh

install --mode=0755 --owner=0 --group=0 --directory /usr/lib/untrustedhost/libexec
for f in ./libexec/* ; do
  b="${f#./libexec/}"
  install --mode=0755 --owner=0 --group=0 "${f}" "/usr/lib/untrustedhost/libexec/${b}"
done

# imd hooks
install --mode=0755 --owner=0 --group=0 --directory /usr/lib/untrustedhost/imd
for f in ./run-parts/* ; do
  b="${f#./run-parts/}"
  install --mode=0755 --owner=0 --group=0 "${f}" "/usr/lib/untrustedhost/imd/${b}"
done

# dhcp templates
install --mode=0755 --owner=0 --group=0 --directory /usr/lib/untrustedhost/dhcp-boot-templates
for f in ./dhcp-boot-templates/* ; do
  b="${f#./dhcp-boot-templates/}"
  install --mode=0755 --owner=0 --group=0 "${f}" "/usr/lib/untrustedhost/dhcp-boot-templates/${b}"
done

# configs
install --mode=0755 --owner=0 --group=0 --directory /etc/untrustedhost
install --mode=0644 --owner=0 --group=0 ./conf/imd.conf /etc/untrustedhost/imd.conf

install --mode=0755 --owner=0 --group=0 --directory /etc/anycast-healthchecker.d
install --mode=0644 --owner=0 --group=0 ./confstub/anycast-hc.conf /etc/anycast-healthchecker.d/anycast-hc.conf

install --mode=0755 --owner=0 --group=0 --directory /usr/lib/untrustedhost/tmpfiles-factory
install --mode=0644 --owner=0 --group=0 ./conf/bird.conf /usr/lib/untrustedhost/tmpfiles-factory/bird.conf
install --mode=0644 --owner=0 --group=0 ./tmpfiles-factory/anycast-prefixes.conf /usr/lib/untrustedhost/tmpfiles-factory/anycast-prefixes.conf
install --mode=0644 --owner=0 --group=0 ./tmpfiles.d/anycast-healthchecker.conf /etc/tmpfiles.d/anycast-healthchecker.conf
install --mode=0644 --owner=0 --group=0 ./tmpfiles.d/bird.conf /etc/tmpfiles.d/bird.conf

install --mode=0644 --owner=0 --group=0 ./udev/30-imd-cd.rules /etc/udev/rules.d/30-imd-cd.rules

# systemd control files (it should be systemd/system here, oops)
for f in ./systemd/* ; do
  [[ -f "${f}" ]] || continue
  b="${f#./systemd/}"
  case "${f}" in
    *.service|*.timer) install --mode=0644 --owner=0 --group=0 "${f}" "/etc/systemd/system/${b}"
  esac
done

# eh, we handle drop-ins separate. no real reason ;)
for d in ./systemd/* ; do
  [[ -d "${d}" ]] || continue
  b="${d#./systemd/}"
  install -d --mode=0755 --owner=0 --group=0 "/etc/systemd/system/${b}"
  for f in "${d}"/* ; do
    [[ -f "${f}" ]] || continue
    n="${f#"./systemd/${b}/"}"
    case "${f}" in
      *.conf) install --mode=0644 --owner=0 --group=0 "${f}" "/etc/systemd/system/${b}/${n}"
    esac
  done
done

# the ntp-wait service is *always* installed...
systemctl enable ntp-wait.service

# versioning
[[ -f "REVISION" ]] && {
  read -r gv < REVISION
  install --mode=0755 --owner=0 --group=0 --directory /usr/lib/untrustedhost/facts.d
  install --mode=0644 --owner=0 --group=0 /dev/null /usr/lib/untrustedhost/facts.d/imd.txt
  printf 'imd_coderev=%s\n' "${gv}" > /usr/lib/untrustedhost/facts.d/imd.txt
}
