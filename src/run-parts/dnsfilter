#!/usr/bin/env bash

dns_xml="$(xmlstarlet sel -t -c /metadata/dnsfilter "${IMD_PATH}")"

[[ "${dns_xml}" ]] || exit 0

mkdir -p /run/untrustedhost/unbound.conf.d

# server address?
server_addrs=("$(xmlstarlet sel -t -v dnsfilter/address/@ipv4 <<<"${dns_xml}")"
              "$(xmlstarlet sel -t -v dnsfilter/address/@ipv6 <<<"${dns_xml}")")

{
  printf 'server:\n'

  # cache is set to 0 because the forwarder does it
  printf '  cache-max-ttl: %s\n' '0'

  # listening addresses
  [[ "${server_addrs[*]}" ]] && {
    for address_list in "${server_addrs[@]}" ; do
      for address in ${address_list} ; do
        printf '  interface: %s\n' "${address}"
      done
    done
  }

  # acl
  printf '  access-control: %s %s\n' '0.0.0.0/0' 'allow'

  # logging
  printf '  logfile: %s\n' '/var/log/unbound.log'
  printf '  verbosity: %s\n' '1'
  printf '  log-queries: %s\n' 'yes'

  # we turn _off_ DNSSEC here. if you want that, insert a DNSSEC-respecting forwarder.
  printf '  module-config: "%s"\n' 'iterator'

  printf 'include: "%s"\n' \
    '/run/untrustedhost/unbound.conf.d/*.conf' \
    '/etc/unbound/unbound.conf.d/*.conf'
} > /run/untrustedhost/unbound.conf

# forwarders?
fwdct="$(xmlstarlet sel -t -v 'count(/dnsfilter/forwarder)' <<<"${dns_xml}")"
[[ "${fwdct}" ]] || fwdct=0
[[ "${fwdct}" -gt 0 ]] && {
  printf 'forward-zone:\n'
  printf '  name: "%s"\n' '.'
  printf '  forward-first: %s\n' 'yes'
  while [[ "${fwdct}" -gt 0 ]] ; do
    printf '  forward-addr: %s\n' "$(xmlstarlet sel -t -v '/dnsfilter/forwarder['"${fwdct}"']' <<<"${dns_xml}")"
    fwdct=$((fwdct - 1))
  done
} > /run/untrustedhost/unbound.conf.d/upstream.conf

# zones?
zonect="$(xmlstarlet sel -t -v 'count(/dnsfilter/zone)' <<<"${dns_xml}")"
[[ "${zonect}" ]] || zonect=0
while [[ "${zonect}" -gt 0 ]] ; do
  printf 'forward-zone:\n'
  printf '  name: "%s"\n' "$(xmlstarlet sel -t -v '/dnsfilter/zone['"${zonect}"']/@name' <<<"${dns_xml}")"
  printf '  forward-first: %s\n' 'yes'
  for entity in $(xmlstarlet sel -t -v '/dnsfilter/zone['"${zonect}"']/@ns' <<<"${dns_xml}") ; do
    printf '  forward-addr: %s\n' "${entity}"
  done
  zonect=$((zonect - 1))
done > /run/untrustedhost/unbound.conf.d/zones.conf

# wire the remote for netdata monitoring
{
  printf 'remote-control:\n'
  printf '  control-enable: %s\n' 'yes'
  printf '  control-interface: %s\n' '/run/unbound.sock'
} > /run/untrustedhost/unbound.conf.d/remote.conf

mkdir -p /run/untrustedhost/netdata
{
  printf 'jobs:\n'
  printf '  - name: %s\n' 'local'
  printf '    address: %s\n' '/run/unbound.sock'
} > /run/untrustedhost/netdata/unbound.conf

chmod a+r /var/lib/unbound/root.key
touch /var/log/unbound.log
chown unbound:unbound /var/log/unbound.log

unbound-checkconf /run/untrustedhost/unbound.conf || exit 1

[[ -d /etc/netdata/go.d ]] && {
  ln -sf /run/untrustedhost/netdata/unbound.conf /etc/netdata/go.d/unbound.conf
}

systemd-run --on-active=5s systemctl start untrustedhost-unbound
