#!/usr/bin/env bash

# determine servers for dhcrelay and wire it up
dhcrelays="$(xmlstarlet sel -t -v 'metadata/dhcprelay/destination' "${IMD_PATH}" | xargs printf '%s ')"
dhcrelays="${dhcrelays:0:-1}"

# next - do we have a dhcp server in ospf mode?
dhcpd_address_block="$(xmlstarlet sel -t -v 'metadata/dhcpserver/address/@ipv4' "${IMD_PATH}")"

[[ "${dhcpd_address_block}" ]] && {
  # require dhcpd to start before dhcrelay
  mkdir -p /run/systemd/system/untrustedhost-dhcrelay.service.d
  {
    printf '[%s]\n' 'Unit'
    printf '%s=untrustedhost-dhcpd.service\n' 'BindsTo'
  } > /run/systemd/system/untrustedhost-dhcrelay.service.d/10-req-dhcpd.conf

  # attempt to start after dhcpd (NOTE: modified dhcpd's unit drop-ins!)
  mkdir -p /run/systemd/system/untrustedhost-dhcpd.service.d
  {
    printf '[%s]\n' 'Service'
    printf 'ExecStartPost=-%s\n' '/usr/bin/systemd-run --on-active=3s systemctl start untrustedhost-dhcrelay'
  } > /run/systemd/system/untrustedhost-dhcpd.service.d/zz-start-dhclient.conf

  # calculate dhcpd server ip and add to list
  dhcpsrv="$(ipcalc "${dhcpd_address_block}"| awk '$1 == "HostMax:" { print $2 }')"  
  dhcrelays="${dhcrelays} ${dhcpsrv}"
}

# remove any duplicate ips
uniq_dhcrelays=''
for ipaddr in ${dhcrelays} ; do
  case "${uniq_dhcrelays}" in
    *" ${ipaddr} "*) : ;;
    *) uniq_dhcrelays="${ipaddr} ${uniq_dhcrelays}" ;;
  esac
done
[[ "${uniq_dhcrelays}" ]] && uniq_dhcrelays="${uniq_dhcrelays:0:-1}"

# if we have some relays, wire up the relay agent now.
[[ "${uniq_dhcrelays}" ]] && {
  mkdir -p /run/systemd/system/untrustedhost-dhcrelay.service.d
  {
    printf '[%s]\n' 'Service'
    printf 'Environment=SERVERS="%s"\n' "${uniq_dhcrelays}"
  } > /run/systemd/system/untrustedhost-dhcrelay.service.d/00-targets.conf

  {
    printf '[%s]\n' 'Unit'
    printf 'Description=%s\n' 'DHCP Relay Agent'
    printf 'Wants=%s\n' 'network-online.target'
    printf 'After=%s\n' 'network-online.target' 'systemd-networkd.service' 'untrustedhost-dhcpd.service'
    printf '[%s]\n' 'Service'
    printf 'Type=%s\n' 'forking'
    # shellcheck disable=SC2016
    printf 'ExecStart=%s\n' '/usr/sbin/dhcrelay -pf /run/dhcrelay.pid $INTERFACES $SERVERS'
    printf 'PIDFile=%s\n' '/run/dhcrelay.pid'
  } > /run/systemd/system/untrustedhost-dhcrelay.service

  # if we didn't have a dhcpd to hang off of, wire a start timer
  systemd-run --on-active=10s systemctl start untrustedhost-dhcrelay
}

# reload systemd
systemctl daemon-reload
