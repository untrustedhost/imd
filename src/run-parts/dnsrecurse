#!/usr/bin/env bash

# configure a recursing dns cache using BIND

case "${1:-init}" in
  # handle forwarder updates here...
  fwdupdate)
    :
  ;;
  # called by imd
  init)
    xmlcfg="$(xmlstarlet sel -t -c '/metadata/dnsrecurse' "${IMD_PATH}")"
    [[ "${xmlcfg}" ]] || exit 0
    <<<"${xmlcfg}" cat > /run/untrustedhost/dnsrecurse.xml
  ;;
esac

[[ -f /run/untrustedhost/dnsrecurse.xml ]] || exit 0

mkdir -p /run/untrustedhost/bind
{
  printf 'include "%s";\n' \
    '/run/untrustedhost/bind/named.conf.options' \
    '/run/untrustedhost/bind/named.conf.forwarding' \
    '/etc/bind/named.conf.default-zones'
} > /run/untrustedhost/bind/named.conf

: > /run/untrustedhost/bind/named.conf.options
fwdct="$(xmlstarlet sel -t -v 'count(/dnsrecurse/forwarder)' '/run/untrustedhost/dnsrecurse.xml')"
[[ "${fwdct}" ]] || fwdct=0
{
  printf 'options {\n'
  printf '  directory "%s";\n' '/var/cache/bind'
  printf '  dnssec-validation %s;\n' 'auto'
  listen_on="$(xmlstarlet sel -t -v '/dnsrecurse/address/@ipv4' '/run/untrustedhost/dnsrecurse.xml')"
  [[ "${listen_on}" ]] && {
    printf '  listen-on port 53 {\n'
    printf '    %s;\n' "${listen_on}"
    printf '  };\n'
  }
  [[ "${fwdct}" -gt 0 ]] && {
    printf '  forwarders {\n'
    while [[ "${fwdct}" -gt 0 ]] ; do
      printf '    %s;\n' "$(xmlstarlet sel -t -v '/dnsrecurse/forwarder['"${fwdct}"']' '/run/untrustedhost/dnsrecurse.xml')"
      fwdct=$((fwdct - 1))
    done
    printf '  };\n'
  }
  printf '};\n'
  printf 'statistics-channels {\n'
  printf '  inet 127.0.0.1 port 8888 allow { 127.0.0.1; };\n'
  printf '};\n'
} > /run/untrustedhost/bind/named.conf.options

: > /run/untrustedhost/bind/named.conf.forwarding
zonect="$(xmlstarlet sel -t -v 'count(/dnsrecurse/zone)' '/run/untrustedhost/dnsrecurse.xml')"
[[ "${zonect}" ]] || zonect=0
{
  while [[ "${zonect}" -gt 0 ]] ; do
    printf 'zone "%s" {\n' "$(xmlstarlet sel -t -v '/dnsrecurse/zone['"${zonect}"']/@name' '/run/untrustedhost/dnsrecurse.xml')"
    printf '  type forward;\n'
    printf '  forward only;\n'
    printf '  forwarders {\n'
    for fwdr in $(xmlstarlet sel -t -v '/dnsrecurse/zone['"${zonect}"']/@ns' '/run/untrustedhost/dnsrecurse.xml') ; do
      printf '    %s;\n' "${fwdr}"
    done
    printf '  };\n'
    printf '};\n'
    zonect=$((zonect - 1))
  done
} > /run/untrustedhost/bind/named.conf.forwarding

# sanity check...
named-checkconf /run/untrustedhost/bind/named.conf || exit 1

# okay, for init, schedule a start. otherwise bounce it.
case "${1:-init}" in
  fwdupdate)
    systemctl restart untrustedhost-bind
  ;;
  init)
    systemd-run --on-active=5s systemctl start untrustedhost-bind
  ;;
esac
